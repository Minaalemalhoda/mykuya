object Versions {
    const val code = 1
    const val name = "1.0"
    const val minSdk = 16
    const val targetSdk = 29
    const val compileSdk = 29

    const val android = "4.0.1"
    const val kotlin = "1.3.72"
    const val appCompat = "1.1.0"
    const val coreKtx = "1.1.0"
    const val material = "1.2.1"
    const val constraintLayout = "1.1.3"
    const val navigation = "2.3.0"
    const val retrofit = "2.6.0"
    const val koin = "2.1.6"
    const val coroutine = "1.3.9"
    const val mapBox = "9.5.0"
    const val fragment = "1.3.0-alpha05"

    //Test libraries
    const val junit = "4.12"
    const val junitExt = "1.1.1"
    const val espresso = "3.2.0"
    const val androidArchitecture = "2.1.0"
    const val mockk = "1.9"
    const val coroutinesTest = "1.3.0-M2"
}

object Dependencies {
    const val kotlin = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${Versions.kotlin}"
    const val coreKtx = "androidx.core:core-ktx:${Versions.coreKtx}"
    const val appCompat = "androidx.appcompat:appcompat:${Versions.appCompat}"
    const val material = "com.google.android.material:material:${Versions.material}"
    const val constraintLayout = "androidx.constraintlayout:constraintlayout:${Versions.constraintLayout}"
    const val navigationFragmentKtx = "androidx.navigation:navigation-fragment-ktx:${Versions.navigation}"
    const val navigationUiKtx = "androidx.navigation:navigation-ui-ktx:${Versions.navigation}"
    const val navigationUi = "androidx.navigation:navigation-ui:${Versions.navigation}"
    const val navigationFragment = "androidx.navigation:navigation-fragment:${Versions.navigation}"
    const val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofit}"
    const val retrofitGsonConverter = "com.squareup.retrofit2:converter-gson:${Versions.retrofit}"
    const val koinScope = "org.koin:koin-androidx-scope:${Versions.koin}"
    const val koinViewModel = "org.koin:koin-androidx-viewmodel:${Versions.koin}"
    const val coroutine = "org.jetbrains.kotlinx:kotlinx-coroutines-core:${Versions.coroutine}"
    const val mapBox = "com.mapbox.mapboxsdk:mapbox-android-sdk:${Versions.mapBox}"
    const val fragment = "androidx.fragment:fragment-ktx:${Versions.fragment}"

    //Test libraries
    const val junitExt = "androidx.test.ext:junit:${Versions.junitExt}"
    const val junit = "junit:junit:${Versions.junit}"
    const val espresso = "androidx.test.espresso:espresso-core:${Versions.espresso}"
    const val archTesting = "androidx.arch.core:core-testing:${Versions.androidArchitecture}"
    const val mockk = "io.mockk:mockk:${Versions.mockk}"
    const val coroutinesTest = "org.jetbrains.kotlinx:kotlinx-coroutines-test:${Versions.coroutinesTest}"

}

object Plugins {
    const val kotlinPlugin = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlin}"
    const val androidPlugin = "com.android.tools.build:gradle:${Versions.android}"
    const val safeArgsPlugin = "androidx.navigation:navigation-safe-args-gradle-plugin:${Versions.navigation}"
}