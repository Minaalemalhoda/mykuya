package com.example.mykuya.presentation.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.mykuya.domain.*
import com.example.mykuya.domain.repository.HomeRepository
import com.example.mykuya.domain.repository.UserRepository
import io.mockk.MockKAnnotations
import io.mockk.Runs
import io.mockk.coEvery
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.just
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

class HomeViewModelTest {

    @RelaxedMockK
    lateinit var homeRepository: HomeRepository

    @RelaxedMockK
    lateinit var userRepository: UserRepository

    @get:Rule
    val rule: TestRule = InstantTaskExecutorRule()

    private val testDispatcher = TestCoroutineDispatcher()

    private val mockLocation = Location(35f, 52f, "")
    private val mockUser = User("name", mockLocation)
    private val news = News(123, "first news", "first desc")
    private val serviceCategory = ServiceCategory("first service", 123, false)

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        Dispatchers.setMain(testDispatcher)
        coEvery { homeRepository.getServiceCategories() } coAnswers {
            delay(200)
            listOf(serviceCategory)
        }
        coEvery { homeRepository.getNews() } coAnswers {
            delay(300)
            listOf(news)
        }
        coEvery { userRepository.getUser() } coAnswers {
            delay(400)
            mockUser
        }
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
    }

    private fun createViewModel() = HomeViewModel(homeRepository, userRepository)

    @Test
    fun `test when viewModel starts its state should be loading`() =
        runBlocking {
            val viewModel = createViewModel()

            assertEquals(true, viewModel.state.isLoading())
        }

    @Test
    fun `test when some data are fetched but not all of them, state should stay at loading`() =
        runBlocking {
            val viewModel = createViewModel()

            delay(350)
            assertEquals(true, viewModel.state.isLoading())
        }

    @Test
    fun `test when all data are fetched, state should not be loading anymore`() =
        runBlocking {
            val viewModel = createViewModel()

            delay(550)
            assertEquals(false, viewModel.state.isLoading())
        }

    @Test
    fun `test when news are fetched, state should be updated and contains them`() =
        runBlocking {
            val viewModel = createViewModel()

            delay(550)
            assert(viewModel.state.news is LoadableData.Loaded)
        }

    @Test
    fun `test when serviceCategories are fetched, state should be updated and contains them`() =
        runBlocking {
            val viewModel = createViewModel()

            delay(550)
            assert(viewModel.state.serviceCategories is LoadableData.Loaded)
        }

    @Test
    fun `test when user data are fetched, state should be updated and contains them`() =
        runBlocking {
            val viewModel = createViewModel()

            delay(550)
            assert(viewModel.state.user is LoadableData.Loaded)
        }

    @Test
    fun `test when news data fetching is failed, state should be updated to failed`() =
        runBlocking {
            coEvery { homeRepository.getNews() } throws Exception()

            val viewModel = createViewModel()

            delay(550)
            assert(viewModel.state.news is LoadableData.Failed)
        }

    @Test
    fun `test when serviceCategories data fetching is failed, state should be updated to failed`() =
        runBlocking {
            coEvery { homeRepository.getServiceCategories() } throws Exception()

            val viewModel = createViewModel()

            delay(550)
            assert(viewModel.state.serviceCategories is LoadableData.Failed)
        }

    @Test
    fun `test when user data fetching is failed, state should be updated to failed`() =
        runBlocking {
            coEvery { userRepository.getUser() } throws Exception()

            val viewModel = createViewModel()

            delay(550)
            assert(viewModel.state.user is LoadableData.Failed)
        }

    @Test
    fun `test when location gets updated, state should be updated to loading as well`() =
        runBlocking {
            coEvery { userRepository.updateUserLocation(any()) } just Runs

            val viewModel = createViewModel()
            delay(450)
            viewModel.updateLocation(mockLocation)

            assertEquals(true, viewModel.state.isLoading())
        }
}