package com.example.mykuya.domain.repository

import com.example.mykuya.domain.Location
import com.example.mykuya.domain.User

interface UserRepository {
    suspend fun getUser(): User
    suspend fun updateUserLocation(newLocation: Location)
}
