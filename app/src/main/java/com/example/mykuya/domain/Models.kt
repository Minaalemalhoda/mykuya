package com.example.mykuya.domain

import android.os.Parcelable
import androidx.annotation.DrawableRes
import kotlinx.android.parcel.Parcelize

data class ServiceCategory(
    val title: String,
    @DrawableRes val iconDrawable: Int,
    val isFeatured: Boolean = false
)

data class User(val name: String, var location: Location)

@Parcelize
data class Location(val latitude: Float, val longitude: Float, val address: String): Parcelable

data class News(@DrawableRes val imageDrawable: Int, val title: String, val description: String)

sealed class LoadableData<out T> {
    object Loading : LoadableData<Nothing>()
    data class Loaded<T>(val data: T? = null) : LoadableData<T>()
    data class Failed(val error: String) : LoadableData<Nothing>()

    fun getValue(): T? = (this as? Loaded<T>)?.data
}