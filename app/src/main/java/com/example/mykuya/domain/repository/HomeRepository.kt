package com.example.mykuya.domain.repository

import com.example.mykuya.domain.News
import com.example.mykuya.domain.ServiceCategory

interface HomeRepository {
    suspend fun getServiceCategories(): List<ServiceCategory>
    suspend fun getNews(): List<News>
}
