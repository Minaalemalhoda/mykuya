package com.example.mykuya.domain.repository

import com.example.mykuya.domain.Location

interface MapboxRepository {
    suspend fun getLocationInfo(location: Location, token: String): String
}