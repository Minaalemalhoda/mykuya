package com.example.mykuya.presentation.di.module

import com.example.mykuya.data.MapboxApi
import com.example.mykuya.data.repository.MapboxRepositoryImp
import com.example.mykuya.domain.repository.MapboxRepository
import com.example.mykuya.presentation.viewmodel.LocationPickerViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit

val locationModule = module {
    single {
        get<Retrofit>().create(MapboxApi::class.java)
    }
    single<MapboxRepository> { MapboxRepositoryImp(get()) }
    viewModel {
        LocationPickerViewModel(get())
    }
}