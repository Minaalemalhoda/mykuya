package com.example.mykuya.presentation.ui.adapter

import androidx.recyclerview.widget.DiffUtil
import com.example.mykuya.R
import com.example.mykuya.domain.News
import com.example.mykuya.presentation.utils.setDrawable
import kotlinx.android.synthetic.main.item_news.view.*

val newsAdapter = BaseAdapter(
    layoutRes = R.layout.item_news,
    bind = {
        newsItemHeaderImage.setDrawable(it.imageDrawable)
        newsItemDescriptionText.text = it.description
        newsItemTitleText.text = it.title
    },
    diffCallback = object : DiffUtil.ItemCallback<News>() {
        override fun areItemsTheSame(oldItem: News, newItem: News) = oldItem.title == newItem.title
        override fun areContentsTheSame(oldItem: News, newItem: News) = oldItem == newItem

    })