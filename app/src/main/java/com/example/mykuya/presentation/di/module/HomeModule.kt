package com.example.mykuya.presentation.di.module

import com.example.mykuya.data.HomeApi
import com.example.mykuya.data.UserApi
import com.example.mykuya.data.repository.HomeRepositoryImp
import com.example.mykuya.data.repository.UserRepositoryImp
import com.example.mykuya.domain.repository.HomeRepository
import com.example.mykuya.domain.repository.UserRepository
import com.example.mykuya.presentation.viewmodel.HomeViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit

val homeModule = module {
    single {
        get<Retrofit>().create(UserApi::class.java)
    }
    single {
        get<Retrofit>().create(HomeApi::class.java)
    }
    single<HomeRepository> { HomeRepositoryImp(get()) }
    single<UserRepository> { UserRepositoryImp(get()) }
    viewModel {
        HomeViewModel(get(), get())
    }
}