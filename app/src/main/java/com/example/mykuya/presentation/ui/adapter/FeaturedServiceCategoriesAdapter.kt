package com.example.mykuya.presentation.ui.adapter

import androidx.recyclerview.widget.DiffUtil
import com.example.mykuya.R
import com.example.mykuya.domain.ServiceCategory
import com.example.mykuya.presentation.utils.setDrawable
import com.example.mykuya.presentation.utils.setWidth
import kotlinx.android.synthetic.main.item_servicecategory.view.*

val featuredServiceCategoriesAdapter = BaseAdapter(
    layoutRes = R.layout.item_servicecategory,
    bind = {
        setWidth(100)
        serviceCategoryItemIcon.setDrawable(it.iconDrawable)
        serviceCategoryItemTitle.text = it.title
    },
    diffCallback = object : DiffUtil.ItemCallback<ServiceCategory>() {
        override fun areItemsTheSame(
            oldItem: ServiceCategory,
            newItem: ServiceCategory
        ) = oldItem.title == newItem.title

        override fun areContentsTheSame(
            oldItem: ServiceCategory,
            newItem: ServiceCategory
        ) = oldItem == newItem
    })