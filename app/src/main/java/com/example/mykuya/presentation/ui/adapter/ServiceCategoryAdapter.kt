package com.example.mykuya.presentation.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.mykuya.R
import com.example.mykuya.domain.ServiceCategory
import com.example.mykuya.presentation.utils.setDrawable
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_servicecategory.*

class ServiceCategoryAdapter : RecyclerView.Adapter<ServiceCategoryViewHolder>() {

    private var state: State = State.COLLAPSE
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    var items = listOf<ServiceCategory>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ServiceCategoryViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_servicecategory, parent, false)
        return ServiceCategoryViewHolder(view, parent.context)
    }

    override fun getItemCount(): Int = if (state == State.COLLAPSE && items.size > COLLAPSE_MODE_ITEM_COUNT) COLLAPSE_MODE_ITEM_COUNT else items.size

    override fun onBindViewHolder(holder: ServiceCategoryViewHolder, position: Int) {
        holder.bindData(items[position])
    }

    fun toggleState(onToggleStateListener: (State) -> Unit) {
        state = if (state == State.COLLAPSE) State.EXPAND else State.COLLAPSE
        onToggleStateListener.invoke(state)
    }

    enum class State {
        COLLAPSE,
        EXPAND
    }

    companion object {
        const val COLLAPSE_MODE_ITEM_COUNT = 6
    }
}

class ServiceCategoryViewHolder(item: View, private val context: Context) :
    RecyclerView.ViewHolder(item), LayoutContainer {

    override val containerView: View?
        get() = itemView

    fun bindData(serviceCategory: ServiceCategory) {
        serviceCategoryItemTitle.text = serviceCategory.title
        serviceCategoryItemIcon.setDrawable(serviceCategory.iconDrawable)
    }

}
