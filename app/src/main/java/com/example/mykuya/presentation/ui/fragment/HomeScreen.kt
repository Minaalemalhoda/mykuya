package com.example.mykuya.presentation.ui.fragment

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.setFragmentResultListener
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.example.mykuya.R
import com.example.mykuya.domain.Location
import com.example.mykuya.domain.News
import com.example.mykuya.domain.ServiceCategory
import com.example.mykuya.domain.User
import com.example.mykuya.presentation.ui.adapter.ServiceCategoryAdapter
import com.example.mykuya.presentation.ui.adapter.featuredServiceCategoriesAdapter
import com.example.mykuya.presentation.ui.adapter.newsAdapter
import com.example.mykuya.presentation.utils.AutoFitGridLayoutManager
import com.example.mykuya.presentation.utils.dpToPx
import com.example.mykuya.presentation.viewmodel.HomeViewModel
import kotlinx.android.synthetic.main.homeview_featured_servicecategories.*
import kotlinx.android.synthetic.main.screen_home.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class HomeScreen : BaseFragment(R.layout.screen_home) {

    private val viewModel: HomeViewModel by viewModel()

    private val serviceCategoryAdapter = ServiceCategoryAdapter()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpServiceCategories(view)
        setUpNews()
        homeLocationText.setOnClickListener {
            navigateToPickLocationPage()
        }
        homeToggleCategoriesButton.setOnClickListener {
            onToggleServiceCategoriesClicked()
        }

        with(viewModel) {
            homeDataState.observe(this@HomeScreen.viewLifecycleOwner, Observer { state ->
                when {
                    state.isFailed() -> {
                        toggleErrorMode()
                    }
                    state.isLoading() -> {
                        toggleLoading(true)
                    }
                    else -> {
                        toggleLoading(false)
                        showUserInfo(state.user.getValue())
                        setServiceCategories(state.serviceCategories.getValue())
                        setNewsInfo(state.news.getValue())
                    }
                }
            })
        }
    }

    private fun navigateToPickLocationPage() {
        setFragmentResultListener(LocationPickerScreen.REQUEST_CODE) { _, bundle ->
            bundle.getParcelable<Location>(LocationPickerScreen.LOCATION_KEY)?.let {
                viewModel.updateLocation(it)
            }
        }
        findNavController().apply {
            navigate(HomeScreenDirections.navigateToLocationPickerScreen())
        }
    }

    private fun toggleLoading(enable: Boolean) {
        homePageLoading.isVisible = enable
    }

    private fun toggleErrorMode() {
        toggleLoading(false)
        Toast.makeText(context, "Something Went Wrong.", Toast.LENGTH_SHORT).show()
    }

    private fun setUpServiceCategories(view: View) {
        featuredCategories.adapter = featuredServiceCategoriesAdapter
        homeServiceCategories.adapter = serviceCategoryAdapter
        homeServiceCategories.layoutManager = AutoFitGridLayoutManager(
            requireContext(), view.dpToPx(SERVICE_CATEGORY_ITEM_WIDTH)
        )
    }

    private fun setUpNews() {
        homeNewsList.adapter = newsAdapter
    }

    private fun setNewsInfo(news: List<News>?) {
        newsAdapter.submitList(news)
    }

    private fun onToggleServiceCategoriesClicked() {
        serviceCategoryAdapter.toggleState { state ->
            toggleCategoriesButtonIcon(state)
        }
    }

    private fun toggleCategoriesButtonIcon(state: ServiceCategoryAdapter.State) {
        homeToggleCategoriesButton.setIconResource(
            if (state === ServiceCategoryAdapter.State.COLLAPSE)
                R.drawable.ic_baseline_keyboard_arrow_down_24
            else
                R.drawable.ic_baseline_keyboard_arrow_up_24
        )
    }

    private fun setServiceCategories(services: List<ServiceCategory>?) {
        services.takeUnless { it.isNullOrEmpty() }?.let {
            serviceCategoryAdapter.items = it
            setFeaturedServiceCategories(it)
        }
    }

    private fun setFeaturedServiceCategories(list: List<ServiceCategory>) {
        list.filter { it.isFeatured }.takeIf { it.isNotEmpty() }?.let {
            featuredServiceCategoriesAdapter.submitList(it)
        }
    }

    private fun showUserInfo(user: User?) {
        user?.let { data ->
            homeWelcomeText.text = getString(R.string.home_welcome_text, data.name)
            homeLocationText.text = user.location.address
        }

    }

    companion object {
        const val SERVICE_CATEGORY_ITEM_WIDTH = 120
    }
}