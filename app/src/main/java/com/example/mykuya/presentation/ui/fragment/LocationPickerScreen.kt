package com.example.mykuya.presentation.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.setFragmentResult
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.example.mykuya.R
import com.example.mykuya.domain.LoadableData
import com.example.mykuya.domain.Location
import com.example.mykuya.presentation.viewmodel.LocationPickerViewModel
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.camera.CameraPosition
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback
import com.mapbox.mapboxsdk.maps.Style
import kotlinx.android.synthetic.main.screen_location_picker.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class LocationPickerScreen : BaseFragment(R.layout.screen_location_picker), OnMapReadyCallback {

    private var mapboxMap: MapboxMap? = null

    private val viewModel: LocationPickerViewModel by viewModel()

    private var selectedPlace: String = "UNKNOWN"

    override fun onMapReady(mapboxMap: MapboxMap) {
        this.mapboxMap = mapboxMap
        mapboxMap.uiSettings.isAttributionEnabled = false
        mapboxMap.uiSettings.isLogoEnabled = false
        mapboxMap.setStyle(
            Style.OUTDOORS
        ) { //no-op}
        }

        mapboxMap.cameraPosition = CameraPosition.Builder()
            .target(DEFAULT_CAMERA_POSITION)
            .zoom(DEFAULT_ZOOM_LEVEL)
            .build()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Mapbox.getInstance(requireContext(), getString(R.string.mapbox_access_token))
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mapView.onCreate(savedInstanceState)
        mapView.getMapAsync(this)
        viewModel.placeLiveData.observe(this.viewLifecycleOwner, Observer { state ->
            when (state) {
                is LoadableData.Loading -> {
                    toggleLoading(true)
                }
                is LoadableData.Loaded -> {
                    toggleLoading(false)
                    state.data?.let {
                        selectedPlace = it
                        dismiss()
                    }
                }
                is LoadableData.Failed -> {
                    toggleLoading(false)
                }
            }
        })

        locationPickerSubmitButton.setOnClickListener {
            onSubmitLocationClicked()
        }

    }

    private fun toggleLoading(enable: Boolean) {
        locationPickerSubmitLoading.isVisible = enable
        locationPickerSubmitButton.text = if (enable) "" else getString(R.string.location_picker_button_text)
    }

    private fun onSubmitLocationClicked() {
        val centerPoint = getCenterPoint()
        centerPoint?.let {
            viewModel.getAddress(
                Location(it.latitude.toFloat(), it.longitude.toFloat(), ""),
                getString(R.string.mapbox_access_token)
            )
        }
    }

    private fun getCenterPoint() = mapboxMap?.cameraPosition?.target

    private fun getSelectedLocation(): Location? {
        return getCenterPoint()?.let {
            Location(it.latitude.toFloat(), it.longitude.toFloat(), selectedPlace)
        }
    }

    private fun dismiss() {
        setFragmentResult(REQUEST_CODE, Bundle().apply {
            putParcelable(LOCATION_KEY, getSelectedLocation())
        })
        findNavController().navigateUp()
    }

    override fun onResume() {
        super.onResume()
        mapView?.onResume()
    }

    override fun onStart() {
        super.onStart()
        mapView?.onStart()
    }

    override fun onStop() {
        super.onStop()
        mapView?.onStop()
    }

    override fun onPause() {
        super.onPause()
        mapView?.onPause()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView?.onLowMemory()
    }

    override fun onDestroy() {
        super.onDestroy()
        mapView?.onDestroy()
    }

    companion object {
        const val REQUEST_CODE = "123"
        const val LOCATION_KEY = "LOCATION_KEY"
        val DEFAULT_CAMERA_POSITION = LatLng(35.7, 51.40)
        const val DEFAULT_ZOOM_LEVEL = 12.0
    }
}