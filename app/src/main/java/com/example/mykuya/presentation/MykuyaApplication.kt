package com.example.mykuya.presentation

import android.app.Application
import androidx.appcompat.app.AppCompatDelegate
import com.example.mykuya.presentation.di.module.getMykuyaModules
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class MykuyaApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        initializeKoin()
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
    }

    private fun initializeKoin() {
        startKoin {
            androidContext(this@MykuyaApplication)
            modules(getMykuyaModules())
        }
    }

}
