package com.example.mykuya.presentation.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.extensions.LayoutContainer

class BaseAdapter<T>(
    @LayoutRes val layoutRes: Int,
    private val bind: (View.(T) -> Unit),
    diffCallback: DiffUtil.ItemCallback<T>
) :
    ListAdapter<T, ViewHolder<T>>(diffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder<T> {
        return ViewHolder(getView(layoutRes, parent), bind)
    }

    override fun onBindViewHolder(holder: ViewHolder<T>, position: Int) {
        holder.bind(getItem(position))
    }

    private fun getView(@LayoutRes layout: Int, parent: ViewGroup) =
        LayoutInflater.from(parent.context).inflate(
            layout,
            parent,
            false
        )
}

class ViewHolder<T>(itemView: View, val bind: View.(T) -> Unit) : RecyclerView.ViewHolder(itemView),
    LayoutContainer {

    override val containerView: View
        get() = itemView

    fun bind(data: T) {
        containerView.bind(data)
    }
}