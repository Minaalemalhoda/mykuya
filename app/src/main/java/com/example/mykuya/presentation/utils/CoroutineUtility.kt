package com.example.mykuya.presentation.utils

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

object CoroutineUtility {

    @JvmStatic
    fun coroutineDispatcherProvider() = object : CoroutineDispatcherProvider {
        override fun bgDispatcher(): CoroutineDispatcher {
            return Dispatchers.Default
        }

        override fun ioDispatcher(): CoroutineDispatcher {
            return Dispatchers.IO
        }

        override fun uiDispatcher(): CoroutineDispatcher {
            return Dispatchers.Main
        }

    }

}

interface CoroutineDispatcherProvider {
    fun bgDispatcher(): CoroutineDispatcher
    fun uiDispatcher(): CoroutineDispatcher
    fun ioDispatcher(): CoroutineDispatcher
}
