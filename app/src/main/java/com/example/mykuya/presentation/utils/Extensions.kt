package com.example.mykuya.presentation.utils

import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.annotation.DrawableRes
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat

fun View.dpToPx(dp: Int): Int {
    if (dp == 0) return 0
    return dpToPx(dp.toFloat()).toInt()
}

fun View.dpToPx(dp: Float): Float {
    if (dp == 0f) return 0f
    val metrics = resources.displayMetrics
    return dp * metrics.density
}

fun ImageView.setDrawable(@DrawableRes drawable: Int) {
    setImageDrawable(
        ContextCompat.getDrawable(
            context,
            drawable
        )
    )
}

fun View.setWidth(widthInDp: Int) {
    layoutParams = ViewGroup.LayoutParams(dpToPx(widthInDp), layoutParams.height)

}