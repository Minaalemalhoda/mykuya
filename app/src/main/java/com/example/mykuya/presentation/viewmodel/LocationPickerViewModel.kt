package com.example.mykuya.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.example.mykuya.domain.LoadableData
import com.example.mykuya.domain.Location
import com.example.mykuya.domain.repository.MapboxRepository
import com.example.mykuya.presentation.utils.SingleLiveEvent
import kotlinx.coroutines.launch

class LocationPickerViewModel(private val mapboxRepository: MapboxRepository) : BaseViewModel() {

    private val _placeLiveData = SingleLiveEvent<LoadableData<String>>()
    val placeLiveData: LiveData<LoadableData<String>> = _placeLiveData

    fun getAddress(location: Location, accessToken: String) {
        viewModelScope.launch {
            _placeLiveData.value = LoadableData.Loading
            runCatching {
                mapboxRepository.getLocationInfo(location, accessToken)
            }.fold({
                _placeLiveData.value = LoadableData.Loaded(it)
            }, {
                _placeLiveData.value = LoadableData.Failed("Unable to get location info from Mapbox API.")
            })
        }
    }

}