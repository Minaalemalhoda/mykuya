package com.example.mykuya.presentation.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.mykuya.domain.LoadableData
import com.example.mykuya.domain.Location
import com.example.mykuya.domain.News
import com.example.mykuya.domain.ServiceCategory
import com.example.mykuya.domain.User
import com.example.mykuya.domain.repository.HomeRepository
import com.example.mykuya.domain.repository.UserRepository
import kotlinx.coroutines.launch

class HomeViewModel(
    private val homeRepository: HomeRepository,
    private val userRepository: UserRepository
) : BaseViewModel() {

    @Volatile
    var state = HomeDataState(
        user = LoadableData.Loading,
        serviceCategories = LoadableData.Loading,
        news = LoadableData.Loading
    )
        set(value) {
            if (field != value) {
                field = value
                _homeDataState.postValue(value)
            }

        }

    private val _homeDataState = MutableLiveData(state)
    val homeDataState: LiveData<HomeDataState> = _homeDataState

    init {
        getData()
    }

    private fun getData() {
        getUser()
        getServiceCategories()
        getNews()
    }

    fun updateLocation(location: Location) {
        viewModelScope.launch {
            onBg {
                runCatching { userRepository.updateUserLocation(location) }.fold({
                    resetState()
                    getData()
                }, {
                    Log.d(this@HomeViewModel.javaClass.name, "Unable to update user location.")
                })
            }
        }
    }

    private fun resetState() {
        state = HomeDataState(
            user = LoadableData.Loading,
            serviceCategories = LoadableData.Loading,
            news = LoadableData.Loading
        )
    }

    private fun getNews() {
        if (state.news is LoadableData.Loaded)
            return
        viewModelScope.launch {
            onBg {
                runCatching {
                    homeRepository.getNews()
                }.fold({ news ->
                    state = state.copy(news = LoadableData.Loaded(news))
                }, {
                    state = state.copy(news = LoadableData.Failed("An error occurred during fetching news."))
                })
            }
        }
    }

    private fun getServiceCategories() {
        if (state.serviceCategories is LoadableData.Loaded)
            return
        viewModelScope.launch {
            onBg {
                runCatching {
                    homeRepository.getServiceCategories()
                }.fold({ services ->
                    state = state.copy(
                        serviceCategories = LoadableData.Loaded(
                            services
                        )
                    )
                }, {
                    state = state.copy(
                        serviceCategories = LoadableData.Failed("An error occurred during fetching service categories info.")
                    )

                })
            }
        }
    }

    private fun getUser() {
        if (state.user is LoadableData.Loaded)
            return
        viewModelScope.launch {
            onBg {
                runCatching {
                    userRepository.getUser()
                }.fold({ user ->
                    state = state.copy(user = LoadableData.Loaded(user))
                }, {
                    state = state.copy(
                        user = LoadableData.Failed("An error occurred during fetching user info.")
                    )
                })
            }
        }
    }

}

data class HomeDataState(
    val user: LoadableData<User>,
    val serviceCategories: LoadableData<List<ServiceCategory>>,
    val news: LoadableData<List<News>>
) {

    fun isLoading(): Boolean = user is LoadableData.Loading
            || serviceCategories is LoadableData.Loading
            || news is LoadableData.Loading

    fun isFailed(): Boolean = user is LoadableData.Failed
            || serviceCategories is LoadableData.Failed
            || news is LoadableData.Failed
}