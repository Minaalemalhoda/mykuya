package com.example.mykuya.data.repository

import com.example.mykuya.data.MockData
import com.example.mykuya.data.UserApi
import com.example.mykuya.domain.Location
import com.example.mykuya.domain.User
import com.example.mykuya.domain.repository.UserRepository

class UserRepositoryImp(private val userApi: UserApi) : UserRepository {

    override suspend fun getUser(): User {
        return MockData.mockUser
    }

    override suspend fun updateUserLocation(newLocation: Location) {
        MockData.mockUser.location = newLocation
    }
}