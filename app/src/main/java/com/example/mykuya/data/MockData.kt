package com.example.mykuya.data

import com.example.mykuya.R
import com.example.mykuya.domain.Location
import com.example.mykuya.domain.News
import com.example.mykuya.domain.ServiceCategory
import com.example.mykuya.domain.User

object MockData {
    val mockServiceCategories = listOf(
        ServiceCategory("Cleaning", R.drawable.sc_0, true),
        ServiceCategory("Event Assistant", R.drawable.sc_1),
        ServiceCategory("Office Assistant", R.drawable.sc_2),
        ServiceCategory("Coffee Delivery", R.drawable.sc_3),
        ServiceCategory("Messenger", R.drawable.sc_4),
        ServiceCategory("Assistant on buying", R.drawable.sc_5),
        ServiceCategory("Cash On Delivery", R.drawable.sc_6),
        ServiceCategory("Deep Clean", R.drawable.sc_7),
        ServiceCategory("Queuing Up", R.drawable.sc_8),
        ServiceCategory("Shopping", R.drawable.sc_9, true),
        ServiceCategory("Bills Payment", R.drawable.sc_10),
        ServiceCategory("Massage", R.drawable.sc_11, true),
        ServiceCategory("Flyeing", R.drawable.sc_12),
        ServiceCategory("Dish Washing", R.drawable.sc_13),
        ServiceCategory("Personal Assistant", R.drawable.sc_14),
        ServiceCategory("Car Wash", R.drawable.sc_15),
        ServiceCategory("Pet sitting", R.drawable.sc_16),
        ServiceCategory("Grocery Delivery", R.drawable.sc_17),
        ServiceCategory("Food Delivery", R.drawable.sc_18),
        ServiceCategory("Manicure & Pedicure", R.drawable.sc_19)
    )

    val mockNews = listOf(
        News(
            R.drawable.news_0,
            "How To Use The App",
            "Getting Access to on demand services and ease your life!"
        ),
        News(R.drawable.news_1, "List Your Services On Myku", "Do you Offer Manpowe?")
    )

    var mockUser = User("Mina", Location(35.6892f, 51.3890f, "Saadat Abaad"))
}