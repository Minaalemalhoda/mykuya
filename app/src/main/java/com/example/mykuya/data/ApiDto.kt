package com.example.mykuya.data

import com.google.gson.annotations.SerializedName

data class LocationDto(val lat: Float, val lon: Float) {
    override fun toString(): String {
        return "$lon,$lat.json"
    }
}

data class LocationInfo(@SerializedName("place_name") val placeName: String)

data class LocationInfoResponseDto(@SerializedName("features") val locationInfos: List<LocationInfo>?)