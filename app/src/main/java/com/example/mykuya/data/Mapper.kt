package com.example.mykuya.data

import com.example.mykuya.domain.Location

fun Location.toLocationDto()= LocationDto(latitude, longitude)