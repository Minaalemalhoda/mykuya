package com.example.mykuya.data

import com.example.mykuya.domain.Location
import com.example.mykuya.domain.News
import com.example.mykuya.domain.ServiceCategory
import com.example.mykuya.domain.User
import retrofit2.http.GET
import retrofit2.http.PUT
import retrofit2.http.Path
import retrofit2.http.Query

interface UserApi {

    @GET("user/info")
    suspend fun getUser(): User

    @PUT("user/location")
    suspend fun updateLocation(location: Location)
}

interface HomeApi {

    @GET("service/categories")
    suspend fun getServiceCategories(): List<ServiceCategory>

    @GET("service/news")
    suspend fun getNews(): List<News>
}

interface MapboxApi {

    @GET("https://api.mapbox.com/geocoding/v5/mapbox.places/{location}")
    suspend fun getLocationInfo(
        @Path("location") location: String,
        @Query("access_token") token: String
    ): LocationInfoResponseDto
}
