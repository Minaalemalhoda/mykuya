package com.example.mykuya.data.repository

import com.example.mykuya.data.HomeApi
import com.example.mykuya.data.MockData
import com.example.mykuya.domain.News
import com.example.mykuya.domain.ServiceCategory
import com.example.mykuya.domain.repository.HomeRepository
import kotlinx.coroutines.delay

class HomeRepositoryImp(private val homeApi: HomeApi) : HomeRepository {

    override suspend fun getServiceCategories(): List<ServiceCategory> {
        delay(200)
        return MockData.mockServiceCategories
    }

    override suspend fun getNews(): List<News> {
        delay(100)
        return MockData.mockNews
    }
}