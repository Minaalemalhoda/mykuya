package com.example.mykuya.data.repository

import com.example.mykuya.data.MapboxApi
import com.example.mykuya.data.toLocationDto
import com.example.mykuya.domain.Location
import com.example.mykuya.domain.repository.MapboxRepository

class MapboxRepositoryImp(private val mapboxApi: MapboxApi) : MapboxRepository {

    override suspend fun getLocationInfo(newLocation: Location, token: String): String {
        return mapboxApi.getLocationInfo(
            newLocation.toLocationDto().toString(),
            token
        ).locationInfos?.firstOrNull()?.placeName ?: "Unknown Place"
    }
}